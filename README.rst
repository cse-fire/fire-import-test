Fire development
================

Fire is a web-application to collect and grade student labs
in electronic form. It is used and developed by the computer
science and engineering department at Chalmers University of
Technology, in Sweden

Features
--------

- Multiple labs per course
- Automatic grader assignment
- Group/individual labs

Installation
------------

Setting up for development
..........................

Create and activate a new virtual environment.

::

    cd <directory containing this file>
    pip install -e .
    mkdir data
    fireadmin -c development.ini init --admin="John Doe <john@example.com>"
    fireadmin -c development.ini sampledata # optional
    pserve --reload development.ini

When pip-installation fails, instead run ``pip2 install -e .``, ``sudo pip2 install --upgrade pip`` (Ubuntu 14.04) or even ``sudo pip2 install -e .``.

The test development version is running under ``http://127.0.0.1:5000/lbs/``.

As long as the ``fire.debug`` flag is enabled, you will find an e-mail with the
password for admin user ``john.doe@example.com`` in the ``./data/mail/new/``
directory.

Compiling assets (coffescript, less -> javascript, css)
.......................................................

::

    cd assets
    npm install .
    ./node_modules/brunch/bin/brunch watch


Contribute
----------

- Source Code: https://bitbucket.org/cse-fire/fire
- Issue Tracker: https://bitbucket.org/cse-fire/fire/issues
- CI: https://bitbucket.org/cse-fire/fire/addon/pipelines/home
- Documentation: http://readthedocs.org/projects/cse-fire/

Pull requests are welcome. As a general rule, you should base your
changes against the develop_ branch (do check out the `branching model`_).

.. _develop: https://bitbucket.org/cse-fire/fire/branch/develop
.. _branching model: http://nvie.com/posts/a-successful-git-branching-model/

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: cse-fire@google-groups.com

License
-------

The project is licensed under the BSD license_.

.. _license: https://bitbucket.org/cse-fire/fire/raw/master/LICENSE