"""
Custom events to be used with the pyramid event system.  Events are usefull to
decouple part of the application.
"""


class NewSubmission(object):
    def __init__(self, submission, request):
        self.submission = submission
        self.request = request
