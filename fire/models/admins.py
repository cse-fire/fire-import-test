from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Text

from pyramid.security import Allow, ALL_PERMISSIONS

from .users import User


class Admin(User):
    __tablename__ = 'admins'
    __mapper_args__ = {'polymorphic_identity': 'admin'}
    email = Column(Text, ForeignKey('users.email'), primary_key=True)

    __acl__ = [(Allow, 'role:admin', ALL_PERMISSIONS)]
