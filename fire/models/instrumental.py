from sqlalchemy.schema import Column
from sqlalchemy.types import DateTime, Text, Boolean, PickleType, TypeDecorator

from datetime import datetime, timedelta

from ..security import random_hex_string
from .meta import Base, DBSession

class FileCap(Base):
    __tablename__ = 'filecaps'

    id = Column(Text, primary_key=True)
    is_archive = Column(Boolean, nullable=False, default=False)
    filename = Column(Text, nullable=False)
    contents = Column(PickleType, nullable=False)
    issued = Column(DateTime, nullable=False, default=datetime.now)
    expires = Column(DateTime, nullable=False)
    ip_address = Column(Text)

    def __init__(self, ttl=60):
        self.id = random_hex_string(32)
        self.issued = datetime.now()
        self.expires = self.issued + timedelta(seconds=ttl)

    @classmethod
    def issue_single(cls, path, name, ttl=60):
        """Issues a filecap for a single file. The path to the file
        is stored in the contents field."""
        fc = cls(ttl)
        fc.is_archive = False
        fc.filename = name
        fc.contents = path
        DBSession().add(fc)
        return fc

    @classmethod
    def issue_archive(cls, paths, name, ttl=60):
        """Issues a filecap for an archive of files (created on download).
        The contents field will be set to a list of pairs, each containing
        a full fileystem path to a file as well as the path of that file
        within the archive."""
        fc = cls(ttl)
        fc.is_archive = True
        fc.filename = name
        fc.contents = paths[:]
        DBSession().add(fc)
        return fc


class Markdown(object):
    """Text meant to be interpreted as Markdown
    """
    def __init__(self, plain_text):
        self.plain_text = plain_text

    def __str__(self):
        return self.plain_text

    def __repr__(self):
        return "Markdown(%s)" % (repr(self.plain_text),)

    def __len__(self):
        return len(self.plain_text)

    def source(self):
        return self.plain_text


class MarkdownType(TypeDecorator):
    """Type for columns of the database meant to store text to be interpreted as
    Markdown
    """
    impl = Text
    python_type = Markdown

    def process_bind_param(self, value, dialect):
        return value.plain_text if value else None

    def process_result_value(self, value, dialect):
        return Markdown(value) if value else None
