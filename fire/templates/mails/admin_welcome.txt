Hello ${name},

We are pleased to inform you that the fire instance for your course
"${course}" is ready.  You can visit it by navigating to the URL:

  ${fireurl}

You have been registered on this fire instance as a grader.

Log-in with the following credential to start setting-up your course:

email address: ${email}
password:      ${password}


-- 
The Fire development team
