import re
from email.utils import parseaddr

from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound

from .. import mail
from ..security import pwgen
from fire.models import DBSession, User, Grader, GraderWorkload, Lab, Settings


@view_defaults(route_name='admin_graders', permission='has_role:grader')
class AdminGradersView(object):
    """
    This view takes care of operation on the set of graders.
    This include displaying, adding and removing graders.
    """

    def __init__(self, request):
        self.request = request

    @view_config(renderer='fire:templates/admin_graders.html',
                 request_method="GET")
    def list_view(self):
        return dict(graders=Grader.get_all(), labs=Lab.get_all())

    @view_config(request_method='POST', request_param='newgrader', check_csrf=True)
    def add_grader(self):
        new_grader = self.request.params['newgrader']

        if not new_grader:
            return HTTPFound(self.request.route_path('admin_graders'))

        name, email = parseaddr(new_grader)

        if name and email:
            if User.get(email, dbsession=DBSession):
                self.request.flash_fire(type='error',
                                        msg="A user with the email %s is already registered in this course" % email)
                return HTTPFound(self.request.route_path('admin_graders'))

            try:
                names = name.split(' ', 1)
                first_name = names.pop(0)
                last_name = names.pop(0)

                grader = Grader(email, first_name=first_name, last_name=last_name)
                password = pwgen()
                grader.password = password
                DBSession.add(grader)
                mail.sendmail(
                    self.request, [email], 'Your password',
                    'fire:templates/mails/new_grader.txt',
                    {
                        'name': grader.name,
                        'email': grader.email,
                        'course': Settings.get('course_name'),
                        'password': password,
                        'url': self.request.route_url('root')
                    }
                )
                self.request.flash_fire(msg="An account activation link has been sent to %s" % email)
                return HTTPFound(self.request.route_path('admin_graders'))
            except IndexError:
                pass

        else:
          self.request.flash_fire(type='error',
                                  msg="Wrong format for grader's name! "
                                      "Please use 'Bob Bobsson <bob@chalmers.se>' "
                                      "or 'bob@chalmers.se (Bob Bobsson)'.")
        return HTTPFound(self.request.route_path('admin_graders'))

    @view_config(request_method='POST', request_param='action', check_csrf=True)
    def update_grader_workload(self):
        for key, value in self.request.params.items():
            match = re.search('workload\[(\d+)\]\[([^]]+)\]', key)
            if match:
                lab = Lab.get(int(match.group(1)))
                grader = Grader.get(match.group(2))
                assert lab is not None and grader is not None

                DBSession.query(GraderWorkload).filter(
                    GraderWorkload.lab_id == lab.id,
                    GraderWorkload.grader_id == grader.id).delete()

                weight = int(value)
                if weight > 0:
                    DBSession.add(GraderWorkload(lab, grader, weight))

        return HTTPFound(self.request.route_path('admin_graders'))
