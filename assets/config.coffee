exports.config =
  # See docs at http://brunch.readthedocs.org/en/latest/config.html.
  modules:
    definition: false
    wrapper: (path, data) ->
      if data.match /^\s*define\s*\(\s*\[/
        data.replace(/^(\s*define\s*\()/, "$1#{path}, ")
      else
        """
define(#{path}, ['require', 'jquery'], function(require, $) {
  #{data}
});

      """
  paths:
    public: '../fire/static'
  files:
    javascripts:
      joinTo:
        'js/views.js': /^app\/views/
        'js/admin.js': /^app\/admin/
        'js/lib.js': /^app\/lib/
        'js/vendor.js': /^vendor/
      order:
        before: [
          'vendor/scripts/require.js'
          'vendor/scripts/console-helper.js'
          'vendor/scripts/jquery-1.7.2.js'
          'vendor/scripts/jquery.ui.widget.js'
          'vendor/scripts/jquery.iframe-transport.js.js'
          'vendor/scripts/jquery.fileupload.js'
          'vendor/scripts/bootstrap/bootstrap-transition.js'
          'vendor/scripts/bootstrap/bootstrap-alert.js'
          'vendor/scripts/bootstrap/bootstrap-button.js'
          'vendor/scripts/bootstrap/bootstrap-carousel.js'
          'vendor/scripts/bootstrap/bootstrap-collapse.js'
          'vendor/scripts/bootstrap/bootstrap-dropdown.js'
          'vendor/scripts/bootstrap/bootstrap-modal.js'
          'vendor/scripts/bootstrap/bootstrap-tooltip.js'
          'vendor/scripts/bootstrap/bootstrap-popover.js'
          'vendor/scripts/bootstrap/bootstrap-scrollspy.js'
          'vendor/scripts/bootstrap/bootstrap-tab.js'
          'vendor/scripts/bootstrap/bootstrap-typeahead.js'
          'vendor/scripts/bootstrap/bootstrap-affix.js'
        ]

    stylesheets:
      joinTo:
        'css/styles.css': /^(app|vendor)/

  # Enable or disable minifying of result js / css files.
  # minify: true
