$ ->
  $('#grader-workload .weight').change ->
    lab = $(this).parent().data 'lab'
    grader = $(this).parent().data 'grader'

    graders = $('#grader-workload td[data-lab=' + lab + ']')
    total = 0
    graders.each ->
        weight = parseInt($(this).find("input").val())
        if isNaN weight
          weight = 0
          $(this).find("input").val(0)
        total += weight
    if total > 0
        $('#lab-th-' + lab).removeClass 'understaffed'
        graders.removeClass 'understaffed'
    else
        $('#lab-th-' + lab).addClass 'understaffed'
        graders.addClass 'understaffed'
    graders.each ->
        input = $(this).find("input")
        weight = parseFloat(input.val())
        if isNaN weight
          weight = 0
          input.val(0)
        percentage = if total == 0 then 0 else Math.round(100 * weight / total)
        $(this).val weight
        $(this).find("label").html percentage
        if weight > 0
          input.removeClass 'unassigned'
        else
          input.addClass 'unassigned'