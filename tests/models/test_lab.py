import pytest

from fire.models import Lab


def test_auto_slug():
    """ Test that a slug is automatically created from the title if none is
    given """
    l = Lab(title="Do the thing!")
    assert l.slug == 'do-the-thing'
