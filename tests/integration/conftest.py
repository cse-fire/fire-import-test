import pytest
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from fire.models import Base

@pytest.fixture
def dbsession():
    engine = create_engine('sqlite:///:memory:', echo=False)
    Base.metadata.create_all(engine)
    session = sessionmaker(bind=engine)
    def fin():
        session.close()
    return session()
