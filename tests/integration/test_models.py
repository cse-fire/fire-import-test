import pytest
from sqlalchemy.exc import IntegrityError

from fire.models import (
    User, Student, Grader, Group, EmailVerification, CourseCode,
    Lab, GraderWorkload
    )


class TestCourseCode():

    def test_str(self):
        cc = CourseCode(code='TST101')
        assert 'TST101' == str(cc.code)

    def test_init_not_blank(self, dbsession):
        """ Test that the course code cannot be blank """
        cc = CourseCode(code='')
        with pytest.raises(IntegrityError):
            dbsession.add(cc)
            dbsession.flush()

    def test_cannod_delete_code_if_student(self, dbsession):
        """ Test that, if there is student with a particular course code,
        it's not possible to delete the code from the database.
        """
        cc = CourseCode(code='TST101')
        dbsession.add(cc)
        s = Student('test')
        s.course = 'TST101'
        dbsession.add(s)
        with pytest.raises(IntegrityError):
            (dbsession.query(CourseCode)
                .filter(CourseCode.code == 'TST101')
                .delete())


class TestUser():

    def test_pwproperty(self):
        # Using grader here as we cannot save User in the db
        u = Grader('userid')
        assert u.password == ""
        u.password = 'hunter2'
        assert u.password == ""

    def test_check_password_good(self, dbsession):
        dbsession.add(Grader('userid', password="hunter2"))
        assert User.check_password('userid', 'hunter2', dbsession)

    def test_check_password_bad(self, dbsession):
        dbsession.add(Grader('userid', password="hunter2"))
        assert not User.check_password('userid', 'nope!', dbsession)

    def test_check_password_unknown_user(self, dbsession):
        assert not User.check_password('foobar', 'hunter2', dbsession)

    def test_repr(self):
        user = User("userid")
        assert repr(user) == "<User userid>"

    def test_unicode(self):
        """
        Test that the __unicode__ method provide usefull information
        """
        user = User("userid")
        user.first_name = "Luc"
        user.last_name = "Skywalker"
        user.email = "iamajedi@tatoin.eu"
        assert unicode(user) == "Luc Skywalker <iamajedi@tatoin.eu>"

    def test_init_mail(self):
        """
        Test that we can initialize a user using a mail address
        """
        user = User("test@test.com")
        assert user.email == "test@test.com"

    def test_init_name_mail(self):
        """
        Test that we can initialize a user using a name <address> string
        of the same format that is accepted by mail servers.
        """
        user = User("bob@test.com", first_name="Bob", last_name="Bobsson")
        assert user.email == "bob@test.com"
        assert user.name == "Bob Bobsson"

    def test_print_empty_name(self):
        """
        Test that when a user has no name set, the ``name`` attribute returns
        none
        """
        user = User('test')
        assert user.name is None


class TestGrader():

    def test_safe_info(self):
        g = Grader('test@example.com', first_name='John', last_name='Doe')
        assert g.safe_info == {
            'course': None, 'id': 'test@example.com',
            'email': 'test@example.com', 'id_number': None,
            'name': 'John Doe', 'role': 'grader'
        }


class TestLab():

    def test_repr(self):
        l = Lab(title='Terraformation')
        assert repr(l) == '<Lab: Terraformation>'


class TestGraderAssignment_init():

    def testCanLeaveOutWeight(self):
        grader = Grader('ms.krabappel@springfield.edu')
        lab = Lab("Rocket building", id=1)
        workload = GraderWorkload(lab, grader)
        assert workload.weight == 1

    def testWeigthCannotBeZero(self, dbsession):
        grader = Grader('ms.krabappel@springfield.edu')
        lab = Lab("Rocket building", id=1)
        with pytest.raises(IntegrityError):
            dbsession.add(GraderWorkload(lab, grader, 0))
            dbsession.flush()


class TestGroups():

    def test_unicode(self):
        group = Group(id=1)
        assert unicode(group) == u"Group 1"


class TestEmailVerification(object):

    def test_default_token(self, dbsession):
        """ Test that a 16 char long default verification token is created """
        emailverification = EmailVerification.create(
            dbsession, email='test@test.com')
        assert 16 == len(emailverification.token)

    def test_valid_token(self, dbsession):
        """ When passed a valid token, the method should return the associated
        email address"""
        EmailVerification.create(
            dbsession, email='test@test.com', token='aaaabbbbccccdddd')
        assert (
            'test@test.com' ==
            EmailVerification.check('aaaabbbbccccdddd', dbsession=dbsession))

    def test_invalid(self, dbsession):
        """ When passed an invalid token, the method should raise a ValueError
        Exception """
        EmailVerification.create(
            dbsession, email='test@test.com', token='aaaabbbbccccdddd')
        with pytest.raises(ValueError):
            EmailVerification.check('0000000000000000', dbsession=dbsession)

    """ The get_unverified method is supposed to return all the emails which
    have not yet been verified. Here verified means that there is a user
    account with that email """

    def test_get_unverified_return_all(self, dbsession):
        """ With only one token in the database and no user, it returns
        this token """
        ev = EmailVerification.create(
            email='test@test.com', dbsession=dbsession)
        assert EmailVerification.get_unverified(dbsession) == [ev]

    def test_get_unverified_exclude_verified(self, dbsession):
       """ Test that it does exclude verified emails """
       ev = EmailVerification.create(
           email='test@test.com', dbsession=dbsession)
       EmailVerification.create(
           email='user@test.com', dbsession=dbsession)
       Student.create(first_name='John', last_name='Doe', email='user@test.com', dbsession=dbsession)
       assert EmailVerification.get_unverified(dbsession) == [ev]
