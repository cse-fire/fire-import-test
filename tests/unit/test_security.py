# coding: utf8
from __future__ import unicode_literals

import pytest

from fire.security import hash_pwd, verify_pwd

PRECOMP_PWD = 'correcthorsebatterystaple'
PRECOMP_HASH = '3ecd988ccd9248f4$sha512$65536$42f4b6a8a5a96779a497c3e19244ec121630fd2351455f1d5680c6439c3a72a6a7d5c83d3a9cdc42ac040c656f153792076002842c2115884082edc405b1ca20'

def test_hash_pwd__same_pwd():
    assert hash_pwd('hunter2') != hash_pwd('hunter2')


def test_hash_pwd__same_pwd_and_salt():
    assert hash_pwd('hunter2', salt='42') == hash_pwd('hunter2', salt='42')


def test_hash_pwd__different_pwd():
    assert hash_pwd('hunter2') != hash_pwd('anotherone')


def test_hash_pwd__unicode():
    """ Shouldn't raise an exception """
    hash_pwd(u'áóéúíçäïœœ')


def test_hash_pwd__precomp():
    assert PRECOMP_HASH == hash_pwd(PRECOMP_PWD, '3ecd988ccd9248f4')


@pytest.mark.parametrize('pw', ('hunter2', 'asdf', 'f;aefi153{?135?%33'))
def test_verify_pwd(pw):
    assert verify_pwd(pw, hash_pwd(pw))


def tests_verify_pwd__precomp():
    assert verify_pwd(PRECOMP_PWD, PRECOMP_HASH)


def test_verify_pwd__invalid():
    with pytest.raises(ValueError):
        verify_pwd('x', 'notvalid')


def test_verify_pwd__rounds_not_integer():
    with pytest.raises(ValueError):
        # The rounds is not an integer here
        verify_pwd('', '3ecd988ccd9248f4$sha512$foo$42f4b6a8a5a96779a497c3e19244ec121630fd2351455f1d5680c6439c3a72a6a7d5c83d3a9cdc42ac040c656f153792076002842c2115884082edc405b1ca20')


def test_verify_pwd__unknown_hashing_algo():
    with pytest.raises(ValueError):
        # Unknown hashing alg
        verify_pwd('', '3ecd988ccd9248f4$fancyhash$65536$42f4b6a8a5a96779a497c3e19244ec121630fd2351455f1d5680c6439c3a72a6a7d5c83d3a9cdc42ac040c656f153792076002842c2115884082edc405b1ca20')
