from utils import login, click_when_able

def test_submit(server, browser, student, lab):
    """ A student submitting to a lab """
    login(server, browser, student)

    browser.find_element_by_id(lab.slug).click()

    browser.find_element_by_id('comment_edit_field').send_keys("Foobaz")
    browser.find_element_by_id('submit-button').click()
    browser.find_element_by_id('confirm-submit-button').click()

    status = (browser.find_element_by_id('submission-1')
                     .find_element_by_class_name('status')
                     .text)
    assert status == "PENDING REVIEW"


def test_resubmit(server, browser, rejectedsubmission):
    """ A student resubmitting to a lab after a rejection """
    student = rejectedsubmission.submitters[0]
    grouplab = rejectedsubmission.lab

    login(server, browser, student)

    browser.find_element_by_id(grouplab.slug).click()

    browser.find_element_by_id('comment_edit_field').send_keys("Foobaz, again")
    browser.find_element_by_id('submit-button').click()
    browser.find_element_by_id('confirm-submit-button').click()

    status = (browser.find_element_by_id('submission-2')
                     .find_element_by_class_name('status')
                     .text)
    assert status == "PENDING REVIEW"
