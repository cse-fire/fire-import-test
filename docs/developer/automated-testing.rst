==================
Automated testing
==================

Automated tests are in :file:`tests` which is divided in three sub-directories:

* :file:`unit`: unit tests, test small amount of code in isolation. Good for
  algorithms, string generation, or generally everything that can be easily
  isolated.  Shouldn't do IO (database, files...) so they should be be fast.
* :file:`integration`: integration tests, get a scratch-dir and an (in memory)
  database.  Given the architecture of pyramid web apps, this is where most
  test will probably end-up.  In particular, this is probably the place for
  view and model tests.
* :file:`end2end`: End-to-end tests (or system tests). Those test a live server
  through a real browser using selenium.  Good coverage but slow and hard to
  debug.


Running the test-suite
======================

There are three ways to run the test suite:

* :command:`./scripts/citest`: this is the script that is used by the continuous
  integration server.  It runs the whole test suite, generate a report in Xunit
  format and hide the browser window in a frame buffer (probably only works in
  linux).
* :command:`tox`: will set-up a virtualenv for the test run and install test
  dependencies.
* :command:`py.test`: you need to install the test dependencies yourself, most
  flexible.

For instance, to run only the end-to-end tests::

    py.test tests/end2end/
