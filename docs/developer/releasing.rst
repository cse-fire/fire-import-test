======================
Creating a new release
======================

This is how to make a release::

    git checkout -b release-VERSION develop
    ## bump version, commit
    ## Test, fix-bugs, repeat
    ## ...
    git checkout master
    git merge --no-ff release-VERSION
    git tag -a VERSION

After that, you want to merge it in develop::

    git checkout develop
    git merge --no-ff release-VERSION

And delete the branch::

    git branch -d release-VERSION


Versionning
===========

The following is an adaptation of http://semver.org/ adapted to our particular
use case: given a version number ``MAJOR.MINOR.PATCH``, increment:

#. MAJOR version when you make backward incompatible API changes (here we are
   talking about an eventual web API, so it will probably no be incremented
   now). Use 2.
#. MINOR version when you make a change to the database schema
#. PATCH version when you make changes that don't touch the database schema

The idea being that ongoing courses can always be upgraded to a new patch
version (changes that don't touch the database) but new minor versions can only
be used for new instances.
