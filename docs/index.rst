.. Fire documentation master file, created by
   sphinx-quickstart on Sun Aug 26 22:01:37 2012.

Welcome to Fire's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 1
   :caption: User documentation

   user/faq
   intro
   setup
   usage
   user/autochecks

.. toctree::
   :maxdepth: 1
   :caption: Developer documentation

   developer/branches
   developer/database
   developer/manual-testing
   developer/automated-testing
   developer/releasing
   developer/server
   developer/deployment
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

